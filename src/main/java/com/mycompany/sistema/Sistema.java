/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.sistema;

import java.io.*;
import java.net.*;

/**
 *
 * @author  aclemente.dev@gmail.com
 */
public class Sistema {

    public static void main(String[] args) {
        System.out.println("versió 0.1 del projecte sistema"); 	
        try {
            InetAddress adreça = InetAddress.getLocalHost();
            String hostname = adreça.getHostName();
            System.out.println("hostname="+hostname);
            System.out.println("Nom de l'usuari: " + System.getProperty("user.name"));
            System.out.println("Carpeta Personal: " + System.getProperty("user.home"));
            System.out.println("Sistema operatiu: " + System.getProperty("os.name"));
            System.out.println("Versió OS: " + System.getProperty("os.version"));

            System.out.println("Creació de la branca 00 del projecte sistema");

            System.out.println("Afegint més codi a la branca00 del projecte sistema");
        }
        catch (IOException e) {
		System.out.println("Exception occurred");
        }  
    }
}
